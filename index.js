const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const app = express();
const MongoClient = require("mongodb").MongoClient;
const ObjectId = require("mongodb").ObjectID;
const port = 4000;

const uri = `mongodb+srv://doctor:s01955298739@cluster0.z2baq.mongodb.net/weddings?retryWrites=true&w=majority`;

require("dotenv").config();

app.use(bodyParser.json());
app.use(cors());

app.get("/", (req, res) => {
  res.send("Hello World!");
});

const client = new MongoClient(
  uri,
  { useUnifiedTopology: true },
  { useNewUrlParser: true },
  { connectTimeoutMS: 30000 },
  { keepAlive: 1 }
);
client.connect((err) => {
  console.log("database connected");
  const userCollection = client.db("eRMG").collection("users");
  const materialPartsCollection = client
    .db("eRMG")
    .collection("materialsParts");
  const RFQCollection = client.db("eRMG").collection("totalRFQ");

  app.post("/addUser", (req, res) => {
    const newUser = req.body;
    console.log(newUser);
    userCollection.insertOne(newUser).then((result) => {
      res.send(result.insertedCount > 0);
    });
  });

  app.get("/fetchUser/:email", (req, res) => {
    console.log(req.params.email);
    userCollection.find({ email: req.params.email }).toArray((err, docs) => {
      console.log(docs[0]);
      res.send(docs && docs[0]);
    });
  });
  app.get("/User/:id", (req, res) => {
    userCollection
      .find({ _id: ObjectId(req.params.id) })
      .toArray((err, documents) => {
        res.send(documents[0]);
      });
  });

  app.patch("/updateUser/:email", (req, res) => {
    const data = req.body.data;
    userCollection
      .updateOne(
        { email: req.params.email },
        {
          $set: {
            userInfo: data,
          },
        }
      )
      .then((result) => {
        res.send(result.modifiedCount > 0);
      });
  });

  app.post("/addRFQ", (req, res) => {
    const RFQ = req.body;

    RFQCollection.insertOne(RFQ)
      .then((result) => {
        res.send(result.insertedCount > 0);
      })
      .catch((err) => {
        console.log(err);
      });
  });
  app.post("/myRFQ", (req, res) => {
    const user = req.body.user;

    RFQCollection.find({ user: user }).toArray((err, documents) => {
      res.send(documents);
    });
  });

  app.patch("/updateMyRfq/:id", (req, res) => {
    const data = req.body.data;
    RFQCollection.updateOne(
      { _id: ObjectId(req.params.id) },
      {
        $set: {
          ProductName: data.ProductName,
          quantity: data.quantity,
          ProductDescription: data.ProductDescription,
          fabricDescription: data.fabricDescription,
          user: data.user,
          budget: data.budget,
          category: data.category,
          productType: data.productType,
          productGroup: data.productGroup,
          sourcingType: data.sourcingType,
          customizedDesign: data.customizedDesign,
          sourcingPurpose: data.sourcingPurpose,
          image: data.image,
        },
      }
    ).then((result) => {
      res.send(result.modifiedCount > 0);
    });
  });

  app.delete("/deleteMyRfq/:id", (req, res) => {
    const RfqID = req.params.id;

    RFQCollection
      .deleteOne({ _id: ObjectId(RfqID) })
      .then((result) => {
        res.send(result.deletedCount > 0);
      });
  });

  app.get("/quotation-Requests", (req, res) => {
    RFQCollection.find({}).toArray((err, documents) => {
      res.send(documents);
    });
  });
  app.get("/singleQuotationDetails/:key", (req, res) => {
    RFQCollection.find({ _id: ObjectId(req.params.key) }).toArray(
      (err, documents) => {
        res.send(documents[0]);
      }
    );
  });

  app.post("/addMaterial", (req, res) => {
    const material = req.body;
    console.log(material);
    materialPartsCollection
      .insertOne(material)
      .then((result) => {
        res.send(result.insertedCount > 0);
      })
      .catch((err) => {
        console.log(err);
      });
  });
  app.post("/materialParts", (req, res) => {
    const user = req.body.user;

    materialPartsCollection.find({ user: user }).toArray((err, documents) => {
      res.send(documents);
    });
  });
  app.get("/singleMaterialPart/:key", (req, res) => {
    materialPartsCollection
      .find({ _id: ObjectId(req.params.key) })
      .toArray((err, documents) => {
        res.send(documents[0]);
      });
  });
  app.patch("/updateMaterialParts", (req, res) => {
    const data = req.body.data;
    const id = data.id;
    const materialName = data.materialName;
    const description = data.description;
    const materialType = data.materialType;
    const weight = data.weight;
    const size = data.size;
    const materialColor = data.materialColor;
    const materialImg = data.materialImg;
    const composition = data.composition;
    const additionalInformation = data.additionalInformation;
    materialPartsCollection
      .updateOne(
        { _id: ObjectId(id) },
        {
          $set: {
            materialName: materialName,
            description: description,
            materialType: materialType,
            weight: weight,
            size: size,
            materialColor: materialColor,
            materialImg: materialImg,
            composition: composition,
            additionalInformation: additionalInformation,
          },
        }
      )
      .then((result) => {
        res.send(result.modifiedCount > 0);
      });
  });

  app.delete("/deleteMaterialPart/:id", (req, res) => {
    const materialId = req.params.id;

    materialPartsCollection
      .deleteOne({ _id: ObjectId(materialId) })
      .then((result) => {
        res.send(result.deletedCount > 0);
      });
  });
});

app.listen(process.env.PORT || port);
